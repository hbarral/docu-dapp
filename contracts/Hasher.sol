//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.10;

import 'hardhat/console.sol';

contract Hasher {
  string theHash;

  event HashStored(string hash);

  constructor(string memory initialHash) {
    theHash = initialHash;
  }

  function getHash() public view returns (string memory) {
    console.log("Hash is '%s'", theHash);
    return theHash;
  }

  function setHash(string calldata newHash) external {
    theHash = newHash;
    emit HashStored(theHash);
  }
}
