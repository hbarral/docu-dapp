import '@nomiclabs/hardhat-ethers'
import { ethers } from 'hardhat'
import { expect } from 'chai'

describe('dummy-hash', function () {
  it('should say dummy-hash', async function () {
    const Hasher = await ethers.getContractFactory('Hasher')
    const hasherContract = await Hasher.deploy('dummy-hash')
    await hasherContract.deployed()
    await hasherContract.setHash('dummy-hash')
    const theHash = await hasherContract.getHash()
    // console.log('res:', theHash)

    expect(theHash).to.be.equal('dummy-hash')
  })
})
