import { useEffect } from 'react'
import './App.css'

import Hasher from './artifacts/contracts/Hasher.sol/Hasher.json'
import { ethers } from 'ethers'

export default function App() {
  const connectWallet = async () => {
    try {
      const { ethereum } = window

      if (!ethereum) {
        alert('Please install MetaMask!')
        return
      }

      const accounts = await ethereum.request({
        method: 'eth_requestAccounts',
      })

      console.log('Connected', accounts[0])
    } catch (error) {
      console.log(error)
    }
  }

  const hashDocument = async () => {
    console.log('storing a hash...')
    let contractAddress = '0xCf7Ed3AccA5a467e9e704C703E8D87F634fB0Fc9'
    // let contractAddress = process.env.CONTRACT_ADDRESS
    const { ethereum } = window

    if (!ethereum) {
      alert('Please install MetaMask!')
      return
    }

    const provider = new ethers.providers.Web3Provider(ethereum).getSigner()
    const contract = new ethers.Contract(contractAddress, Hasher.abi, provider)

    await contract.setHash('some-hash-here-will-be-here')
    const theHash = await contract.getHash()

    // provider.on(provider.filters.HashStored(), function (value) {
    //   console.log('meh:', value)
    // })

    console.log('retrieved hash: ', theHash)
  }

  useEffect(() => {
    connectWallet()
  }, [])

  return (
    <div className="App">
      <h1>Document Hasher</h1>
      <button onClick={hashDocument}> Store Hash </button>
    </div>
  )
}
