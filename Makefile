# SCRIPT ?= $(shell bash -c 'read -p "Script: " script; echo $$script')

node:
	npx hardhat node

compile:
	npx hardhat compile

deploy:
	npx hardhat run scripts/deploy-hasher.ts --network localhost
	# npx hardhat run scripts/$(SCRIPT) --network localhost
	
testing:
	npx hardhat test
