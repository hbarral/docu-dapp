import '@nomiclabs/hardhat-ethers';
import { ethers } from 'hardhat'

async function main() {
const hasherContract = await ethers.getContractFactory("Hasher");
const deployedContract = await hasherContract.deploy("Hello World!");
await deployedContract.deployed();

console.log("Hasher Contract Address:", deployedContract.address);
}

main()
.then(() => process.exit(0))
.catch((error) => {
  console.error(error);
  process.exit(1);
});
